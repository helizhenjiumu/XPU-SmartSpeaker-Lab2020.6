cmake_minimum_required(VERSION 3.1...3.18)
project(SmartSpeaker)

add_executable(tts tts.c auth.c cJSON.c)
target_link_libraries(tts curl)
target_compile_options(tts PRIVATE -Wall -D_GNU_SOURCE)

add_executable(robot robot.c cJSON.c)
target_link_libraries(robot curl)
target_compile_options(robot PRIVATE -Wall)

add_library(snowboy-detect-c-wrapper STATIC snowboy-detect-c-wrapper.cc)
target_compile_options(snowboy-detect-c-wrapper PRIVATE -Wall -D_GLIBCXX_USE_CXX11_ABI=0)
target_link_libraries(snowboy-detect-c-wrapper ${CMAKE_SOURCE_DIR}/libsnowboy-detect.a)

add_executable(snowboy snowboy.c stt.c auth.c cJSON.c)
target_link_libraries(snowboy curl asound cblas snowboy-detect-c-wrapper)
target_compile_options(snowboy PRIVATE -Wall -D_GNU_SOURCE)